/*
 * В цикле необходимо запрашивать строку у пользователя и спрашивать его,
 * добавить ли эту строку в файл
 *
 * y - да
 * n - нет
 * e - выход из цикла
 *
 * По выходу из цикла выводить кол-во добавленных и недобавленных строк.
 */

#include <iostream>
#include <fstream>
using namespace std;

int ID;
int skippedRows;

struct rows{
    int id;
    string newrow;
};

void addrows(){

    char ch1;
    rows rows;
    cout << "\nEnter new text: ";
    cin.get();
    getline(cin, rows.newrow);
    ID++;

    cout << "Do you want to add this text to file? y/n\n";
    cin >> ch1;
    if (ch1 == 'y'){
        ofstream write;
        write.open("file.txt", ios::app);
        write << "\n" << ID;
        write << "\n" << rows.newrow;
        write.close();

        write.open("id.txt");
        write << ID;
        write.close();
    }
    else{
        skippedRows++;
    }

    char ch2;
    cout << "Do you want to add more texts? y/n\n";
    cin >> ch2;

    if (ch2 == 'y'){
        addrows();
    }
    else {
        cout << "\nYour text has been added successfully\n\n";
        cout << "Added rows: " << ID << "\n";
        cout << "Skipped rows: " << skippedRows << "\n";
        return;
    }
}

int main(){

    addrows();

    return 0;
}